#include "cripto.h"

Cripto::Cripto() {
	srand(time(NULL));
}

bool Cripto::validate_e(Bint P, Bint Q, Bint e) {
	Bint ONE( 1 );
	Bint p = P - ONE;
	Bint q = Q - ONE;
	Bint pq = p * q;

	return gcd(pq, e) == ONE;
}

std::pair<Bint, Bint> Cripto::generate_p_q() {
	uint64_t p = get_random();
	while ( ! is_prime(p))
		p += 2;
	
	uint64_t q = get_random();
	while ( ! is_prime(q))
		q += 2;

	return std::make_pair( Bint(p), Bint(q) );
}

Bint Cripto::generate_private_key(Bint P, Bint Q, Bint e) {
	Bint ZERO 	= Bint( 0 );
	Bint ONE 	= Bint( 1 );
	Bint p = P - ONE;
	Bint q = Q - ONE;
	Bint pq = p * q;

	Bint d = ZERO;

	while (true) {
		d = d + pq;
		Bint d_1 = d + ONE;


		std::pair<Bint, Bint> div;
		d_1.div_operation(e, div);

		if (div.second == ZERO)
			return div.first;
	}
	
	return Bint( 0 );
}

Bint Cripto::encrypt(Bint data, Bint public_key, Bint e) {
	return data.spow(e, public_key);
}

Bint Cripto::decrypt(Bint data, Bint private_key, Bint public_key) {
	return data.spow(private_key, public_key);
}

bool Cripto::is_prime(uint64_t num) {
	if (num % 2 == 0)
		return false;
	
	uint64_t d = 3;

	while (d*d <= num) {
		if (num % d == 0)
			return false;

		d += 2;
	}

	return true;
}

// private
Bint Cripto::gcd(Bint& A, Bint& B) {
	Bint ZERO = Bint( 0 );
	
	if (B == ZERO)
		return A;

	Bint mod = A % B;
	return gcd(B, mod);
}

uint64_t Cripto::get_random() {
	uint64_t r = 0;
	
	for (uint8_t i = 0; i < 64; i++) {
		r = r * 2 + rand() % 2;
	}

	r |= 1;

	return r;
}