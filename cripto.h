#include <iostream>
#include <vector>
#include <ctime>
#include "Bint.h"

class Cripto {
public:
	Cripto();
	bool validate_e(Bint P, Bint Q, Bint e);
	std::pair<Bint, Bint> generate_p_q();
	Bint generate_private_key(Bint P, Bint Q, Bint e);

	Bint encrypt(Bint data, Bint public_key, Bint e);
	Bint decrypt(Bint data, Bint private_key, Bint public_key);

private:
	bool is_prime(uint64_t num);
	uint64_t get_random();
	Bint gcd(Bint& A, Bint& B);
};