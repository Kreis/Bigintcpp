#include <iostream>
#include <vector>
#include "Cripto.h"
#include <math.h>

void testing_bint() {
	std::cout << "start testing bint" << std::endl;
	for (uint32_t i = 0; i < 10000; i++) {
		uint64_t a = (rand() % 10000) + 1;
		uint64_t b = (rand() % 10000) + 1;
		uint64_t base = (rand() % 10) + 1;
		uint64_t p = (rand() % 10) + 1;

		if (a < b) {
			uint64_t t = a;
			a = b;
			b = t;
		}

		uint64_t c_sum = a + b;
		uint64_t c_res = a - b;
		uint64_t c_div = a / b;
		uint64_t c_mul = a * b;
		uint64_t c_mod = a % b;
		uint64_t c_pow = pow(base, p);
		uint64_t base_base = base * base;
		uint64_t c_pow_mod = c_pow % base_base;
		uint64_t c_pp = a;
		uint64_t c_mm = b;
		uint64_t c_mul_self = a * a;
		uint64_t c_sum_self = a + a;

		c_pp++;
		c_mm--;

		Bint bint_a(a);
		Bint bint_b(b);
		
		Bint bint_sum = bint_a + bint_b;
		Bint bint_sum_eq = bint_a;
		bint_sum_eq += bint_b;
		Bint bint_res = bint_a - bint_b;
		Bint bint_res_eq = bint_a;
		bint_res_eq -= bint_b;
		Bint bint_div = bint_a / bint_b;
		Bint bint_div_eq = bint_a;
		bint_div_eq /= bint_b;
		Bint bint_mul = bint_a * bint_b;
		Bint bint_mul_eq = bint_a;
		bint_mul_eq *= bint_b;
		Bint bint_mod = bint_a % bint_b;
		Bint bint_mod_eq = bint_a;
		bint_mod_eq %= bint_b;
		Bint bint_base(base);
		Bint bint_p(p);
		Bint bint_pow = bint_base.spow(bint_p);
		Bint bint_mod_base(base_base);
		Bint bint_pow_mod = bint_base.spow(bint_p, bint_mod_base);
		Bint bint_pp = a;
		Bint bint_mm = b;
		bint_pp.pp();
		bint_mm.mm();
		Bint bint_mul_eq_self(a);
		bint_mul_eq_self *= bint_mul_eq_self;
		Bint bint_sum_eq_self(a);
		bint_sum_eq_self += bint_sum_eq_self;

		Bint bint_c_sum(c_sum);
		Bint bint_c_res(c_res);
		Bint bint_c_div(c_div);
		Bint bint_c_mul(c_mul);
		Bint bint_c_mod(c_mod);
		Bint bint_c_pow(c_pow);
		Bint bint_c_pow_mod(c_pow_mod);
		Bint bint_c_pp(c_pp);
		Bint bint_c_mm(c_mm);
		Bint bint_c_mul_eq_self(c_mul_self);
		Bint bint_c_sum_eq_self(c_sum_self);

		std::string s = std::to_string(a);
		Bint bint_string(s);

		if (bint_c_sum != bint_sum) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_sum:\n";
			bint_c_sum.print();
			std::cout << "bint_sum:\n";
			bint_sum.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_res != bint_res) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_res:\n";
			bint_c_res.print();
			std::cout << "bint_res:\n";
			bint_res.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_div != bint_div) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_div:\n";
			bint_c_div.print();
			std::cout << "bint_div:\n";
			bint_div.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_mul != bint_mul) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_mul:\n";
			bint_c_mul.print();
			std::cout << "bint_mul:\n";
			bint_mul.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_mod != bint_mod) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_mod:\n";
			bint_c_mod.print();
			std::cout << "bint_mod:\n";
			bint_mod.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_pow != bint_pow) {
			std::cout << "base: " << base << std::endl;
			std::cout << "p: " << p << std::endl;
			std::cout << "c_pow: " << c_pow << std::endl;
			std::cout << "bint_c_pow:";
			bint_c_pow.print();
			std::cout << "bint_pow:";
			bint_pow.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_sum != bint_sum_eq) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_sum:\n";
			bint_c_sum.print();
			std::cout << "bint_sum_eq:\n";
			bint_sum_eq.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_res != bint_res_eq) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_res:\n";
			bint_c_res.print();
			std::cout << "bint_res_eq:\n";
			bint_res_eq.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_div != bint_div_eq) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_div:\n";
			bint_c_div.print();
			std::cout << "bint_div_eq:\n";
			bint_div_eq.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_mul != bint_mul_eq) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_mul:\n";
			bint_c_mul.print();
			std::cout << "bint_mul_eq:\n";
			bint_mul_eq.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_mod != bint_mod_eq) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_mod:\n";
			bint_c_mod.print();
			std::cout << "bint_mod_eq:\n";
			bint_mod_eq.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_pp != bint_pp) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_pp:\n";
			bint_c_pp.print();
			std::cout << "bint_pp:\n";
			bint_pp.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_mm != bint_mm) {
			std::cout << "a: " << a << std::endl;
			std::cout << "b: " << b << std::endl;
			std::cout << "bint_c_mm:\n";
			bint_c_mm.print();
			std::cout << "bint_mm:\n";
			bint_mm.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_mul_eq_self != bint_mul_eq_self) {
			std::cout << "a: " << a << std::endl;
			std::cout << "a_self: " << c_mul_self << std::endl;
			std::cout << "bint_c_mul_eq_self:\n";
			bint_c_mul_eq_self.print();
			std::cout << "bint_mul_eq_self:\n";
			bint_mul_eq_self.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_sum_eq_self != bint_sum_eq_self) {
			std::cout << "a: " << a << std::endl;
			std::cout << "a_self: " << c_sum_self << std::endl;
			std::cout << "bint_c_sum_eq_self:\n";
			bint_c_sum_eq_self.print();
			std::cout << "bint_sum_eq_self:\n";
			bint_sum_eq_self.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_c_pow_mod != bint_pow_mod) {
			std::cout << "a: " << a << std::endl;
			std::cout << "a_self: " << c_mul_self << std::endl;
			std::cout << "bint_c_pow_mod:\n";
			bint_c_pow_mod.print();
			std::cout << "bint_pow_mod:\n";
			bint_pow_mod.print();

			throw std::runtime_error("not valid operation");
		}
		if (bint_string != bint_a) {
			std::cout << "a: " << a << std::endl;
			std::cout << "bint_string:\n";
			bint_string.print();
			std::cout << "bint_a:\n";
			bint_a.print();

			throw std::runtime_error("not valid constructor");
		}
		
	}

	std::cout << "success" << std::endl;
}

int main(int argc, char** argv) {
	std::clock_t begin_time = std::clock();
	try {
		testing_bint();
	} catch (std::runtime_error& e) {
		std::cout << e.what() << std::endl;
	}
	

	std::cout << float( std::clock () - begin_time ) /  CLOCKS_PER_SEC;
	return 0;
}


